#pragma once

#include "utils/utils_export.h"

#include <atomic>
#include <memory>
#include <functional>
#include <type_traits>
#include <utility>

namespace utils
{

/// \brief Interface for managing memory, can be used in partner with
/// an TypedAllocator wrapper to monitor stl container allocations.
class Allocator
{
public:
  Allocator() = default;
  Allocator(Allocator const&) = delete;
  Allocator& operator=(Allocator const&) = delete;

  /// \brief Allocate memory
  /// \param size         The amount of memory to allocate
  /// \param alignment    The alignment of the allocated data.
  virtual void* allocate(std::size_t size, std::size_t alignment) = 0;

  /// \brief Release memory
  /// \param ptr The pointer to release
  /// \param size The amount of memory to deallocate
  virtual void deallocate(void* ptr, std::size_t size) = 0;

  /// \brief Find the number of bytes used by the memory allocator's allocations.
  virtual std::size_t memory_usage_bytes() const = 0;

  template <typename T> struct CreatedWithExtra
  {
    T* allocated;
    void* extra_allocated;
    std::size_t allocation_size;
  };

  template <typename T, typename... Args>
  CreatedWithExtra<T> create_with_extra(std::size_t extra, Args&&... args)
  {
    auto allocated_size = sizeof(T) + extra;
    auto allocation = allocate(allocated_size, alignof(T));

    try {
      auto obj = new(allocation) T(std::forward<Args&&>(args)...);
      void* extra_ptr = obj + 1;

      return CreatedWithExtra<T>{ obj, extra_ptr, allocated_size };
    }
    catch (...) {
      deallocate(allocation, allocated_size);
      throw;
    }
  }

  template <typename T, typename... Args>
  T *create(Args&&... args)
  {
    return create_with_extra<T>(0, std::forward<Args&&>(args)...).allocated;
  }

  /// \brief Destruct then deallocate the object [ptr]
  /// \param ptr The pointer to release
  /// \param size The amount of memory to deallocate
  template <typename T>
  void destroy(T *ptr, std::size_t allocation_size)
  {
    ptr->~T();
    deallocate(ptr, allocation_size);
  }

  /// \brief Destruct then deallocate the object [ptr], assuming the
  /// allocation was only for a single object (with sizeof(T)).
  /// \param ptr The pointer to release
  template <typename T>
  void destroy(T *ptr)
  {
    destroy(ptr, sizeof(T));
  }

  /// \brief Find if two allocators are equal
  /// \note Default impl assumes object address identity.
  virtual bool equal(Allocator const& other) const
  {
    return this == &other;
  }

  template <class OtherAllocator>
  bool operator==(OtherAllocator const& other) const { return equal(other); }

  template <class OtherAllocator>
  bool operator!=(OtherAllocator const& other) const { return !(*this == other); }
};

template <typename T, typename... Args>
std::unique_ptr<T, std::function<void(T*)>> make_unqiue(
  utils::Allocator& allocator,
  Args&&... args)
{
  auto allocator_ptr = &allocator;
  return std::unique_ptr<T, std::function<void(T*)>>(
    allocator.create<T>(std::forward<Args>(args)...),
    [allocator_ptr](T* val)
    {
      allocator_ptr->destroy(val);
    }
  );
}

template <typename T, typename... Args>
std::unique_ptr<T, std::function<void(T*)>> make_shared(
  utils::Allocator& allocator,
  Args&&... args)
{
  auto allocator_ptr = &allocator;
  return std::shared_ptr<T>(
    allocator.create<T>(std::forward<Args>(args)...),
    [allocator_ptr](T* val)
    {
      allocator_ptr->destroy(val);
    }
  );
}

/// \brief An allocator allocating typed memory, used to allocate types with in std.
template <class T>
class TypedAllocator
{
public:
  typedef T value_type;

  TypedAllocator(Allocator& forwarder)
    : m_forwarder(forwarder)
  {
  }
  template <class U> TypedAllocator(const TypedAllocator<U>& other)
  : m_forwarder(other.forwarder())
  {
  }

  /// \brief Allocate a memory for a number of T objects.
  /// \param count The number of objects to allocate
  T* allocate(std::size_t count)
  {
    return static_cast<T*>(
      m_forwarder.allocate(count * sizeof(T), alignof(T))
    );
  }

  /// \brief Release a number of T objects
  /// \param ptr   The value to release
  /// \param count The number of objects to allocate (must
  ///              equal value passed to allocate)
  void deallocate(T* ptr, std::size_t count)
  {
    m_forwarder.deallocate(ptr, count * sizeof(T));
  }

  template <class OtherAllocator>
  bool operator==(OtherAllocator const& other) const { return m_forwarder.equal(other.m_forwarder); }

  template <class OtherAllocator>
  bool operator!=(OtherAllocator const& other) const { return !(*this == other); }

  Allocator& forwarder() const { return m_forwarder; }

private:
  Allocator& m_forwarder;
};

UTILS_EXPORT Allocator& global_allocator();

}
