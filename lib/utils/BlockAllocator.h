#pragma once

#include "utils/Allocator.h"

#include <boost/intrusive/list.hpp>
#include <boost/thread/synchronized_value.hpp>

namespace utils
{

class BlockAllocatorManager
{
public:
  class Block : public boost::intrusive::list_base_hook<>
  {
  public:
    Block(Block const&) = delete;
    Block& operator=(Block const&) = delete;

    void* allocate_bytes(std::size_t size)
    {
      auto offset = m_used_size_bytes.fetch_add(size);
      if ((offset + size) > m_block_size_bytes)
      {
        return nullptr;
      }

      return m_data + offset;
    }

    static Block* create(Allocator& source, std::size_t size)
    {
      auto block = source.create_with_extra<Block>(size, size, CtorDummy{});
      return block.allocated;
    }

    static void release(Allocator& source, Block* block)
    {
      source.destroy(block, block->m_block_size_bytes + sizeof(Block));
    }

    std::size_t block_size_bytes() const { return m_block_size_bytes; }

  private:
    struct CtorDummy {};

  public:
    Block(std::size_t block_size, CtorDummy)
      : m_block_size_bytes(block_size)
      , m_used_size_bytes(0)
    {
    }

  private:
    std::size_t m_block_size_bytes;
    std::atomic<std::size_t> m_used_size_bytes;

    char m_data[1];
  };
  using BlockList = boost::intrusive::list<Block>;


  /// Create a new block manager used to coordinate
  /// allocation of data blocks to many block allocators.
  BlockAllocatorManager(
    Allocator& forwarder,
    std::size_t block_size_bytes = 4096,
    std::size_t max_cached_bytes = 1024 * 1024
  )
  : m_forwarder(&forwarder)
  , m_cached_bytes(0)
  , m_block_count(0)
  , m_block_size_bytes(block_size_bytes)
  , m_max_cached_bytes(max_cached_bytes)
  {
  }

  ~BlockAllocatorManager()
  {
    clear();
  }
  
  void reset(Allocator& forwarder)
  {
    clear();
    m_forwarder = &forwarder;
  }
  
  void clear()
  {
    m_blocks->clear_and_dispose([&](BlockAllocatorManager::Block* block) {
      Block::release(*m_forwarder, block);
      --m_block_count;
    });
    assert(m_block_count == 0);
  }

  /// Allocate a new block for the manager.
  Block* allocate_block(std::size_t minimum_size)
  {
    {
      auto blocks = m_blocks.synchronize();
      if (blocks->size())
      {
        auto block = &blocks->front();
        if (block->block_size_bytes() >= minimum_size)
        {
          blocks->pop_front();
          return block;
        }
      }
    }

    auto new_block = Block::create(*m_forwarder, std::max(m_block_size_bytes, minimum_size));
    ++m_block_count;
    return new_block;
  }

  /// Return a block to the manager for reuse or destruction.
  void release_block(Block* block)
  {
    if (m_cached_bytes >= m_max_cached_bytes)
    {
      --m_block_count;
      Block::release(*m_forwarder, block);
      return;
    }

    auto blocks = m_blocks.synchronize();
    blocks->push_back(*block);
    m_cached_bytes += block->block_size_bytes();
  }

private:
  BlockAllocatorManager(BlockAllocatorManager const&) = delete;
  BlockAllocatorManager& operator=(BlockAllocatorManager const&) = delete;
  
  Allocator* m_forwarder;

  std::atomic<std::size_t> m_cached_bytes;
  std::atomic<std::size_t> m_block_count;
  boost::synchronized_value<BlockList> m_blocks;

  std::size_t m_block_size_bytes;
  std::size_t m_max_cached_bytes;
};

class BlockAllocator : public Allocator
{

public:
  BlockAllocator(
    BlockAllocatorManager& manager)
  : m_manager(&manager)
  , m_usage_bytes(0)
  , m_current_block(nullptr)
  , m_live_allocations(0)
  {
    allocate_block();
  }

  ~BlockAllocator()
  {
    assert(m_live_allocations == 0);
    assert(m_usage_bytes == 0);
    m_current_block = nullptr;

    m_blocks->clear_and_dispose([&](BlockAllocatorManager::Block* block) {
      m_manager->release_block(block);
    });
  }

  void* allocate(std::size_t size, std::size_t alignment) override
  {
    auto const to_allocate = size + alignment;
    while (true)
    {
      auto allocated = m_current_block.load()->allocate_bytes(to_allocate);
      if (!allocated)
      {
        allocate_block(to_allocate);
        continue;
      }
      
      m_live_allocations += 1;
      m_usage_bytes += size;

      std::size_t size_remaining = to_allocate;
      allocated = std::align(
        alignment,
        size,
        allocated,
        size_remaining
      );

      // should never fail - we over allocate deliberately.
      assert(allocated);

      return allocated;
    }
  }

  void deallocate(void*, std::size_t size) override
  {
    m_live_allocations -= 1;
    m_usage_bytes -= size;

    // Leave all the rest of the data for our destructor to handle.
  }

  std::size_t memory_usage_bytes() const override { return m_usage_bytes; }

private:
  void allocate_block(std::size_t required_size = 0)
  {
    auto expected_current = m_current_block.load();
    auto block = m_manager->allocate_block(required_size);
    assert(block);
    if (!m_current_block.compare_exchange_strong(expected_current, block))
    {
      // there is a new block, but it isn't ours.
      m_manager->release_block(block);
      return;
    }

    m_blocks->push_back(*block);
  }

  BlockAllocatorManager* m_manager;
  std::atomic<std::size_t> m_usage_bytes;

  boost::synchronized_value<BlockAllocatorManager::BlockList> m_blocks;
  std::atomic<BlockAllocatorManager::Block*> m_current_block;
  std::atomic<std::size_t> m_live_allocations;
};

}
