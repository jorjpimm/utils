#include "utils/Allocator.h"

#include <cassert>
#include <cstdlib>
#include <memory>

namespace utils
{

namespace
{

class GlobalAllocator : public Allocator
{
  void* allocate(std::size_t size, std::size_t alignment) override
  {
    // We will allocate enough space to align the pointer
    // and an extra pointer for book keeping
    std::size_t const ptr_overhead = sizeof(void*);
    std::size_t object_size = size + alignment - 1;

    auto unaligned_ptr = std::malloc(object_size + ptr_overhead);

    // Find the aligned pointer
    auto aligned_ptr = (void*)((char*)unaligned_ptr + ptr_overhead);
    aligned_ptr = std::align(
      alignment,
      size,
      aligned_ptr,
      object_size
    );
    assert(aligned_ptr);

    // Now place the value of the unaligned ptr just behind the aligned pointer
    ((void**)aligned_ptr)[-1] = unaligned_ptr;

    // Not accounting for the alignemnt and book keeping size,
    // but its a small constant overhead, not a big deal.
    // and harder to deal with in the destructor...
    m_memory_usage_bytes += size;

    return aligned_ptr;
  }

  void deallocate(void* ptr, std::size_t size) override
  {
    // Find the pointer to the allocated buffer (just behind the aligned pointer)
    void* allocated_ptr = ((void**)ptr)[-1];
    std::free(allocated_ptr);

    m_memory_usage_bytes -= size;
  }

  std::size_t memory_usage_bytes() const override
  {
    return m_memory_usage_bytes.load();
  }

  bool equal(Allocator const& alloc) const override
  {
    // All global allocators are equal.
    return typeid(alloc) == typeid(*this);
  }

  std::atomic<std::size_t> m_memory_usage_bytes;
};

}

Allocator& global_allocator()
{
  static GlobalAllocator g_allocator;
  return g_allocator;
}
}
