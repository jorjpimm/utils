#pragma once

#include "utils/Allocator.h"

#include <string>
#include <unordered_map>
#include <vector>

namespace utils
{
namespace std_helpers
{

using string = ::std::basic_string<char, std::char_traits<char>, TypedAllocator<char>>;
  
template <typename Key, typename Value>
using unordered_map = ::std::unordered_map<
    Key,
    Value,
    std::hash<Key>,
    std::equal_to<Key>,
    TypedAllocator<std::pair<const Key, Value>>
>;

template <typename Value>
using vector = ::std::vector<
  Value,
  TypedAllocator<Value>
>;

}

}
