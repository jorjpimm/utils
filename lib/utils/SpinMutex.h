#pragma once

#include <atomic>

namespace utils
{

class SpinMutex
{
public:
  SpinMutex()
  {
    m_flag.clear();
  }
  
  void lock()
  {
    while (m_flag.test_and_set(std::memory_order_acquire))
    {
      // No contents
    }
  }
  
  void unlock()
  {
    m_flag.clear(std::memory_order_release);
  }
  
private:
  std::atomic_flag m_flag;
};

}