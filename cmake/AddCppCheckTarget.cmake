
find_program (CPPCHECK_COMMAND NAMES "cppcheck")
if (CPPCHECK_COMMAND)
    add_custom_target(cppcheck)
else()
    message(STATUS "No cppcheck binaries found, skipping targets")
endif()

function(add_cppcheck_target target work_dir)
    if (NOT CPPCHECK_COMMAND)
        return()
    endif()

    get_target_property(target_sources ${target} SOURCES)
    get_target_property(target_includes ${target} INCLUDE_DIRECTORIES)
    
    set(cppcheck_command ${CPPCHECK_COMMAND} --std=c++11 --platform=unix64 --language=c++ -j 4 --xml)
    foreach (dir ${target_includes})
        if ("${dir}" STREQUAL "")
            set(dir ".")
        endif()
        set(cppcheck_command ${cppcheck_command} -I ${dir})
    endforeach()

    foreach (file ${target_sources})
        set(cppcheck_command ${cppcheck_command} ${work_dir}/${file})
    endforeach()

    add_custom_target(${target}_cppcheck 
        ${cppcheck_command}
        WORKING_DIRECTORY ${work_dir}
    )

    add_dependencies(cppcheck ${target}_cppcheck)
endfunction()