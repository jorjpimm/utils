
option(SANITISE_TYPE "What type of sanitisation to do, if any" OFF)
if ("${SANITISE_TYPE}" STREQUAL "address")
    message(STATUS "Sanitising using address sanitiser")
    set(CMAKE_CXX_FLAGS "-fsanitize=address -lasan")
elseif ("${SANITISE_TYPE}" STREQUAL "undefined")
    message(STATUS "Sanitising using undefined sanitiser")
    set(CMAKE_CXX_FLAGS "-fsanitize=undefined")
elseif ("${SANITISE_TYPE}" STREQUAL "leak")
    message(STATUS "Sanitising using leak sanitiser")
    set(CMAKE_CXX_FLAGS "-fsanitize=leak")
elseif ("${SANITISE_TYPE}" STREQUAL "thread")
    message(STATUS "Sanitising using thread sanitiser")
    set(CMAKE_CXX_FLAGS "-fsanitize=thread -fPIE -pie")
endif()
