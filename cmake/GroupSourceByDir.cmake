
function(source_group_by_dir target root)
    get_target_property(target_sources ${target} SOURCES)

    foreach(_source IN ITEMS ${target_sources})
        get_filename_component(_source_path "${_source}" DIRECTORY)
        get_filename_component(_source_path "${_source_path}" ABSOLUTE)

        file(RELATIVE_PATH _source_path_rel "${root}" "${_source_path}")
        string(REPLACE "/" "\\" _group_path "${_source_path_rel}")
        source_group("${_group_path}" FILES "${_source}")
    endforeach()
endfunction()
